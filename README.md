# skamand

## About this
Execute of commandline by openssh client.

## Tree view

```
.
├── [-rw-r--r--]  README.md
├── [drwxr-xr-x]  bin
│   ├── [-rwxr-xr-x]  report
│   └── [-rwxr-xr-x]  ssh-by-key
├── [drwxr-xr-x]  host
│   └── [drwxr-xr-x]  192.168.0.1
│       ├── [drwxr-xr-x]  bin
│       │   └── [lrwxr-xr-x]  ssh-by-key -> ../../../bin/ssh-by-key
│       └── [drwxr-xr-x]  lib
│           ├── [lrwxr-xr-x]  000-hostname -> ../../../lib/hostname
│           ├── [lrwxr-xr-x]  001-free -> ../../../lib/free
│           ├── [lrwxr-xr-x]  002-df -> ../../../lib/df
│           ├── [lrwxr-xr-x]  003-date -> ../../../lib/date
│           └── [lrwxr-xr-x]  004-service-status-apache -> ../../../lib/service-status-apache
└── [drwxr-xr-x]  lib
    ├── [-rw-r--r--]  date
    ├── [-rw-r--r--]  df
    ├── [-rw-r--r--]  free
    ├── [-rw-r--r--]  hostname
    └── [-rw-r--r--]  service-status-apache

```

# For example

Don't forget of prepare for ssh config(~/.ssh/config).

```
$ cd ~/
$ mkdir git
$ cd git
$ git clone git@gitlab.com:equsys/skamand.git
$ cd ~/git/skamand
$ mv host/192.168.0.1 host/your-server001
$ cp -rp host/your-server001 host/your-server002
$ ~/git/skamand/bin/report

```
